<?php

header('Content-Type: text/plain; charset=utf-8');

function getgeoinfo($address, $apikey) {
    $config = array(
        'address' => $address,
        'apiurl' => 'https://geocode-maps.yandex.ru/1.x/?geocode=',
        'apikey' => $apikey
    );
    $xml = simplexml_load_file($config["apiurl"] . urlencode($config["address"]) . '&apikey=' . urlencode($config['apikey']) . '&results=1');
    $found = $xml->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found;
    if ($found > 0) {
        $coords = str_replace(' ', ',', $xml->GeoObjectCollection->featureMember->GeoObject->Point->pos);
        $lonlat = explode(",", $coords);
        $xml_district = simplexml_load_file($config["apiurl"] . $coords . '&apikey=' . urlencode($config['apikey']) . '&results=1&kind=district');
        $geoinfo = array(
            'address' => str_replace('', '', $xml->GeoObjectCollection->featureMember->GeoObject->metaDataProperty->GeocoderMetaData->text),
            'lon' => $lonlat[0], //долгота
            'lat' => $lonlat[1], //широта
            'province' => str_replace(' ', ',', $xml->GeoObjectCollection->featureMember->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->AdministrativeArea->AdministrativeAreaName), //Регион (область)
            'area' => str_replace(' ', ',', $xml->GeoObjectCollection->featureMember->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->AdministrativeArea->SubAdministrativeArea->SubAdministrativeAreaName), //Район области
            'locality' => str_replace(' ', ',', $xml->GeoObjectCollection->featureMember->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->AdministrativeArea->SubAdministrativeArea->Locality->LocalityName), //Населенный пункт
            'district' => str_replace(' ', ',', $xml_district->GeoObjectCollection->featureMember->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->AdministrativeArea->SubAdministrativeArea->Locality->DependentLocality->DependentLocalityName) //Район города (или пустую строку, если населенный пункт не делится на районы)
        );
        print_r($geoinfo);
    } else {
        echo 'Адрес "' . $config['address'] . '" не найден!';
    }
}

getgeoinfo("г Тутаев, улица Моторостроителей, д 58", "ba7e79f8-4c2a-4ed7-87c2-9b3d00c8120c");
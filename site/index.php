<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="../../../../favicon.ico">
		<title>Приветственное письмо</title>
		<!-- Bootstrap core CSS-->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap-grid.min.css">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap-reboot.min.css">
		<!-- Place your stylesheet here-->
		<link href="/template_styles.min.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<main>
			<section>
				<div class="container header">
					<div class="row">
						<div class="col-12">
							<div id="header">
								<div class="logo"></div>
								<div class="shapes"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="container content">
					<div class="row">
						<div class="col-12">
							<div class="hello">
								<h2>Здравствуйте, Светлана!</h2>
								<p>Благодарим вас за выбор сервиса подбора недвижимости «Мой вариант»!</p>
							</div>
							<h2>Сделайте два простых шага к квартире мечты</h2>
							<div class="step_item">
								<h3>Добавьте в систему ваш объект</h3>
								<p>(квартиру, комнату, дом), который вы хотите продать или обменять.</p><a href="#">Читайте, как добавить объект →</a>
							</div>
							<div class="step_item">
								<h3>Опишите жилье, которое вы хотите купить или получить при обмене.</h3>
								<p>Подробно укажите параметры желаемого жилья — это поможет подобрать именно то, что вы хотите.</p><a href="#">Читайте, как добавить запрос →</a>
							</div>
						</div>
						<div class="col-12">
							<h2>А дальше...</h2>
							<p>Система подберет для вас подходящие варианты. А также будет уведомлять о появлении новых объектов, соответствующих вашему запросу.</p>
							<p>Свяжитесь с владельцами объекта для обмена или покупки недвижимости. Вы можете использовать телефон или внутреннюю</p>
						</div>
					</div>
				</div>
			</section>
			<section>
				<div class="container content">
					<div class="row">
						<div class="col-12">
							<h2>Узнайте больше о сервисе <br>«Мой вариант»</h2>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="more_item more_item_service"><a href>Описание всех возможностей сервиса</a></div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="more_item more_item_faq"><a href>Ответы на часто задаваемые вопросы</a></div>
						</div>
					</div>
				</div>
			</section>
			<section id="question">
				<div class="container content">
					<div class="row">
						<div class="col-12">
							<div id="question_block">
								<div class="row">
									<div class="col-sm-8 col-xs-12">
										<h2>Есть вопросы? <br>Обратитесь в службу поддержки!</h2>
										<p>По телефону +7 (4852) 58-17-58 <br>или через форму обратной связи</p>
									</div>
								</div>
								<div class="lady"><span>Мы будем рады помочь Вам!</span></div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</main>
		<footer>
			<div class="container content">
				<div class="row">
					<div class="col-12">
						<h4>Сервис подбора недвижимости «Мой Вариант»</h4>
						<p>
							Вы получили это письмо, потому что подписаны на рассылку в сервисе «Мой вариант». Вы можете управлять настройками рассылки в <a href>личном кабинете.</a></p><a href="">Связаться с нами</a>
						<div class="social_link"><a class="social_link_item social_link_item__vk" href="#"></a><a class="social_link_item social_link_item__fb" href="#"></a></div>
					</div>
				</div>
			</div>
		</footer>
	</body>
</html>